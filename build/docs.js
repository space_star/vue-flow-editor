const $utils = require('./build.utils')
const publicConfig = require('./build.public')

module.exports = {
    publicPath: 'vue-flow-editor',
    outputDir: 'docs',
    devServer: {
        port: '4488'
    },
    pages: {
        index: {
            entry: $utils.resolve('doc/main.ts'),
            template: 'public/index.html',
            filename: 'index.html',
            title: 'vue-flow-editor',
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    },
    configureWebpack: {
        externals: {
            vue: 'Vue',
        },
        resolve: {
            alias: {
                '@vue/composition-api': $utils.resolve('public/lib/vue-composition-api.umd.js')
            },
        },
    },
}