const $utils = require('./build.utils')

const argv = $utils.argv

if (argv.composition) {
    return module.exports = require('./build.composition-api')
}

if (argv.g6) {
    return module.exports = require('./build.g6')
}

if (!argv.release) {
    return module.exports = require('./docs')
} else {
    return module.exports = require('./release')
}
