export const AppData = {
    "nodes": [
        {
            "id": "123456",
            "x": 590,
            "y": 100,
            "text": "广告宣传",
            "desc": "通过广告短频宣传",
            "activity": "advertisement",
        },
        {
            "id": "2323456789",
            "x": 1020,
            "y": 100,
            "text": "优惠券",
            "desc": "发送奖励优惠券",
            "activity": "coupon",
        },
        {
            "data": {},
            "id": "start-node",
            "label": "开始",
            "shape": "ellipse",
            "x": 380,
            "y": 100
        },
        {
            "data": {},
            "id": "1588848310120",
            "label": "主管审批",
            "shape": "rect",
            "x": 380,
            "y": 180
        },
        {
            "data": {},
            "id": "1588848322179",
            "label": "经理审批",
            "shape": "rect",
            "x": 380,
            "y": 260
        },
        {
            "data": {},
            "id": "1588848338211",
            "label": "金额>2万",
            "shape": "diamond",
            "x": 380,
            "y": 340
        },
        {
            "data": {},
            "id": "1588848351476",
            "label": "财务打款",
            "shape": "rect",
            "x": 380,
            "y": 460
        },
        {
            "data": {},
            "id": "end-node",
            "label": "结束",
            "shape": "ellipse",
            "x": 380,
            "y": 540
        },
        {
            "data": {},
            "id": "1588848397511",
            "label": "VIP审批",
            "shape": "rect",
            "x": 590,
            "y": 340
        },
        {
            "data": {},
            "id": "1588848436694",
            "label": "金额>10万",
            "shape": "diamond",
            "x": 780,
            "y": 340
        },
        {
            "data": {},
            "id": "1588848449431",
            "label": "CEO审批",
            "shape": "rect",
            "x": 960,
            "y": 460
        }
    ],
    "edges": [
        {
            "source": "start-node",
            "sourceAnchor": 2,
            "target": "1588848310120",
            "targetAnchor": 0
        },
        {
            "source": "1588848310120",
            "sourceAnchor": 2,
            "target": "1588848322179",
            "targetAnchor": 0
        },
        {
            "source": "1588848322179",
            "sourceAnchor": 2,
            "target": "1588848338211",
            "targetAnchor": 0
        },
        {
            "source": "1588848338211",
            "sourceAnchor": 2,
            "target": "1588848351476",
            "targetAnchor": 0
        },
        {
            "source": "1588848351476",
            "sourceAnchor": 2,
            "target": "end-node",
            "targetAnchor": 0
        },
        {
            "source": "1588848338211",
            "sourceAnchor": 3,
            "target": "1588848397511",
            "targetAnchor": 1
        },
        {
            "source": "1588848436694",
            "sourceAnchor": 3,
            "target": "1588848449431",
            "targetAnchor": 0
        },
        {
            "source": "1588848449431",
            "sourceAnchor": 1,
            "target": "1588848351476",
            "targetAnchor": 3
        },
        {
            "source": "1588848436694",
            "sourceAnchor": 2,
            "target": "1588848351476",
            "targetAnchor": 3
        },
        {
            "source": "1588848397511",
            "sourceAnchor": 3,
            "target": "1588848436694",
            "targetAnchor": 1
        },
        {
            "source": "start-node",
            "sourceAnchor": 3,
            "target": "123456",
            "targetAnchor": 1
        },
        {
            "source": "123456",
            "sourceAnchor": 3,
            "target": "2323456789",
            "targetAnchor": 1
        },
        {
            "source": "2323456789",
            "sourceAnchor": 3,
            "target": "end-node",
            "targetAnchor": 3
        }
    ]
}