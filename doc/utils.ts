export function staticPath(path) {
    return joinPath('/vue-flow-editor', path)
}

export function joinPath(source: string, target: string, separator = '/') {
    if (source.indexOf('/') === source.length - 1) {
        source = source.slice(0, -1)
    }
    if (target.charAt(0) === '/') {
        target = target.slice(1)
    }
    return `${source}${separator}${target}`
}